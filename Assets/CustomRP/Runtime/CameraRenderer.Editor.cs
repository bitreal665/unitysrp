using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace CustomRP.Runtime
{
    public partial class CameraRenderer
    {
        partial void DrawUnsupportedShaders(ScriptableRenderContext context, Camera camera, ref CullingResults cullingResult);

        partial void DrawGizmos(ScriptableRenderContext context, Camera camera);

        partial void PrepareForSceneView(Camera camera);

#if UNITY_EDITOR

        partial void PrepareForSceneView(Camera camera)
        {
            if (camera.cameraType == CameraType.SceneView)
            {
                ScriptableRenderContext.EmitWorldGeometryForSceneView(camera);
            }
        }

        private static ShaderTagId[] LegacyShaderTagIds = {
            new ShaderTagId("Always"),
            new ShaderTagId("ForwardBase"),
            new ShaderTagId("PrepassBase"),
            new ShaderTagId("Vertex"),
            new ShaderTagId("VertexLMRGBM"),
            new ShaderTagId("VertexLM")
        };

        private static Material _errorMaterial;

        partial void DrawUnsupportedShaders(ScriptableRenderContext context, Camera camera, ref CullingResults cullingResult)
        {
            var sortingSettings = new SortingSettings(camera);
            if (_errorMaterial == null)
                _errorMaterial = new Material(Shader.Find("Hidden/InternalErrorShader"));
            var drawingSettings = new DrawingSettings(LegacyShaderTagIds[0], sortingSettings)
            {
                overrideMaterial = _errorMaterial,
            };
            for (var i = 1; i < LegacyShaderTagIds.Length; i++)
                drawingSettings.SetShaderPassName(i, LegacyShaderTagIds[i]);
            var filteringSettings = new FilteringSettings(RenderQueueRange.all);
            context.DrawRenderers(cullingResult, ref drawingSettings, ref filteringSettings);
        }

        partial void DrawGizmos(ScriptableRenderContext context, Camera camera)
        {
            if (Handles.ShouldRenderGizmos())
            {
                context.DrawGizmos(camera, GizmoSubset.PreImageEffects);
                context.DrawGizmos(camera, GizmoSubset.PostImageEffects);
            }
        }

#endif
    }
}
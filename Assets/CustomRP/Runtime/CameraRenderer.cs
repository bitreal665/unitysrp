using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;

namespace CustomRP.Runtime
{
    public partial class CameraRenderer
    {
        private static readonly ShaderTagId UnlitShaderTagId = new ShaderTagId("SRPDefaultUnlit");
        private static readonly ShaderTagId LitShaderTagId = new ShaderTagId("CustomLit");

        private readonly CommandBuffer _commandBuffer;
        private readonly CustomRenderPipelineSettings _settings;
        private readonly Lighting _lighting;
        private readonly Shadows _shadows;

        public CameraRenderer(CustomRenderPipelineSettings settings)
        {
            _commandBuffer = new CommandBuffer();
            _settings = settings;
            _shadows = new Shadows(settings);
            _lighting = new Lighting(_shadows, settings);
        }

        public void Render(ScriptableRenderContext context, Camera camera)
        {
            // draw ui in scene camera

            PrepareForSceneView(camera);

            // cull
            if (!camera.TryGetCullingParameters(out var cullingParameters))
                return;
            cullingParameters.shadowDistance = Mathf.Min(_settings.shadowSettings.maxDistance, camera.farClipPlane);
            var cullingResult = context.Cull(ref cullingParameters);

            // command buffer name

            var commandBufferName = "CommandBuffer";
            if (Application.isEditor)
                commandBufferName = camera.name;    // camera.name allocates!
            _commandBuffer.name = commandBufferName;

            // start sample

            _commandBuffer.BeginSample(commandBufferName);
            context.ExecuteCommandBuffer(_commandBuffer);
            _commandBuffer.Clear();

            // setup shadows

            _shadows.Setup(context, cullingResult);

            // setup lighting

            _lighting.Setup(context, cullingResult);

            // render shadows

            _shadows.Render();

            // setup camera (render target + vp matrix)

            context.SetupCameraProperties(camera);

            // clear

            var clearFlags = camera.clearFlags;
            _commandBuffer.ClearRenderTarget(
                clearFlags <= CameraClearFlags.Depth,
                clearFlags <= CameraClearFlags.Color,
                clearFlags == CameraClearFlags.Color ?
                    camera.backgroundColor.linear : Color.clear);
            context.ExecuteCommandBuffer(_commandBuffer);
            _commandBuffer.Clear();

            // draw opaque

            var sortingSettings = new SortingSettings()
            {
                criteria = SortingCriteria.CommonOpaque,
            };
            var perObjectData = PerObjectData.None;
            if (_settings.useLightsPerObject)
            {
                perObjectData |= PerObjectData.LightData | PerObjectData.LightIndices;
            }
            perObjectData |= PerObjectData.Lightmaps | PerObjectData.LightProbe | PerObjectData.LightProbeProxyVolume;
            var drawingSettings = new DrawingSettings(UnlitShaderTagId, sortingSettings)
            {
                enableInstancing = _settings.useGPUInstancing,
                enableDynamicBatching = _settings.useDynamicBatching,
                perObjectData = perObjectData,
            };
            drawingSettings.SetShaderPassName(1, LitShaderTagId);
            var filteringSettings = new FilteringSettings(RenderQueueRange.opaque);
            context.DrawRenderers(cullingResult, ref drawingSettings, ref filteringSettings);

            // draw unsupported shaders

            DrawUnsupportedShaders(context, camera, ref cullingResult);

            // draw skybox

            if (clearFlags == CameraClearFlags.Skybox)
                context.DrawSkybox(camera);

            // draw transparent

            sortingSettings.criteria = SortingCriteria.CommonOpaque;
            drawingSettings.sortingSettings = sortingSettings;
            filteringSettings.renderQueueRange = RenderQueueRange.transparent;
            context.DrawRenderers(cullingResult, ref drawingSettings, ref filteringSettings);

            // draw gizmos

            DrawGizmos(context, camera);

            // end sample

            _commandBuffer.EndSample(commandBufferName);
            context.ExecuteCommandBuffer(_commandBuffer);
            _commandBuffer.Clear();

            // cleanup shadows

            _shadows.Cleanup();

            // submit

            context.Submit();
        }
    }
}
using UnityEditor;
using UnityEditor.U2D;
using UnityEngine;
using UnityEngine.Rendering;

public class CustomShaderGUI : ShaderGUI
{
    public enum ShadowMode
    {
        On,
        Clip,
        Dither,
        Off,
    }

    private bool Clipping
    {
        set => SetProperty("_Clipping", "_CLIPPING", value);
    }

    private ShadowMode Shadows
    {
        set
        {
            if (SetProperty("_Shadows", (float) value))
            {
                SetKeyword("_SHADOWS_CLIP", value == ShadowMode.Clip);
                SetKeyword("_SHADOWS_DITHER", value == ShadowMode.Dither);
            }
        }
    }

    private bool PremultiplyAlpha
    {
        set => SetProperty("_PremulAlpha", "_PREMULTIPLY_ALPHA", value);
    }

    private BlendMode SrcBlend
    {
        set => SetProperty("_SrcBlend", (float) value);
    }

    private BlendMode DstBlend
    {
        set => SetProperty("_DstBlend", (float) value);
    }

    private bool ZWrite
    {
        set => SetProperty("_ZWrite", value ? 1f : 0f);
    }

    private RenderQueue RenderQueue
    {
        set
        {
            foreach (Material mat in _materials)
                mat.renderQueue = (int)value;
        }
    }

    private MaterialEditor _materialEditor;
    private Object[] _materials;
    private MaterialProperty[] _properties;
    private bool _showPresets;

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
        EditorGUI.BeginChangeCheck();

        base.OnGUI(materialEditor, properties);

        _materialEditor = materialEditor;
        _materials = materialEditor.targets;
        _properties = properties;

        BakedEmission();

        _showPresets = EditorGUILayout.Foldout(_showPresets, "Presets", true);
        if (_showPresets)
        {
            OpaquePreset();
            ClipPreset();
            FadePreset();
            if (HasProperty("_PremulAlpha"))
                TransparentPreset();
        }

        if (EditorGUI.EndChangeCheck())
        {
            TrySetShadowCasterPass();
            CopyLightMappingProperties();
        }

        _materialEditor = null;
        _materials = null;
        _properties = null;
    }

    private void BakedEmission()
    {
        EditorGUI.BeginChangeCheck();
        _materialEditor.LightmapEmissionProperty();
        if (EditorGUI.EndChangeCheck())
        {
            foreach (Material m in _materialEditor.targets)
            {
                m.globalIlluminationFlags &= ~MaterialGlobalIlluminationFlags.EmissiveIsBlack;
                m.globalIlluminationFlags = MaterialGlobalIlluminationFlags.AnyEmissive;
            }
        }
    }

    private void OpaquePreset()
    {
        if (PressButton("Opaque"))
        {
            Clipping = false;
            Shadows = ShadowMode.On;
            PremultiplyAlpha = false;
            SrcBlend = BlendMode.One;
            DstBlend = BlendMode.Zero;
            ZWrite = true;
            RenderQueue = RenderQueue.Geometry;
        }
    }

    private void ClipPreset()
    {
        if (PressButton("Clip"))
        {
            Clipping = true;
            Shadows = ShadowMode.Clip;
            PremultiplyAlpha = false;
            SrcBlend = BlendMode.One;
            DstBlend = BlendMode.Zero;
            ZWrite = true;
            RenderQueue = RenderQueue.AlphaTest;
        }
    }

    private void FadePreset()
    {
        if (PressButton("Fade"))
        {
            Clipping = false;
            Shadows = ShadowMode.Dither;
            PremultiplyAlpha = false;
            SrcBlend = BlendMode.SrcAlpha;
            DstBlend = BlendMode.OneMinusSrcAlpha;
            ZWrite = false;
            RenderQueue = RenderQueue.Transparent;
        }
    }

    private void TransparentPreset()
    {
        if (PressButton("Transparent"))
        {
            Clipping = false;
            Shadows = ShadowMode.Dither;
            PremultiplyAlpha = true;
            SrcBlend = BlendMode.One;
            DstBlend = BlendMode.OneMinusSrcAlpha;
            ZWrite = false;
            RenderQueue = RenderQueue.Transparent;
        }
    }

    private bool PressButton(string label)
    {
        if (GUILayout.Button(label))
        {
            _materialEditor.RegisterPropertyChangeUndo(label);
            return true;
        }
        return false;
    }

    private void TrySetShadowCasterPass()
    {
        var shadows = FindProperty("_Shadows", _properties, false);
        if (shadows == null || shadows.hasMixedValue)
            return;
        var enabled = shadows.floatValue < (float) ShadowMode.Off;
        foreach (Material material in _materials)
            material.SetShaderPassEnabled("ShadowCaster", enabled);
    }

    private void CopyLightMappingProperties()
    {
        var mainTex = FindProperty("_MainTex", _properties, false);
        var baseMap = FindProperty("_BaseMap", _properties, false);
        if (mainTex != null && baseMap != null)
        {
            mainTex.textureValue = baseMap.textureValue;
            mainTex.textureScaleAndOffset = baseMap.textureScaleAndOffset;
        }
        var color = FindProperty("_Color", _properties, false);
        var baseColor = FindProperty("_BaseColor", _properties, false);
        if (color != null && baseColor != null)
        {
            color.colorValue = baseColor.colorValue;
        }
    }

    private bool HasProperty(string name)
    {
        return FindProperty(name, _properties, false) != null;
    }

    private void SetProperty(string name, string keyword, bool val)
    {
        if (SetProperty(name, val ? 1f : 0f))
            SetKeyword(keyword, val);
    }

    private bool SetProperty(string name, float val)
    {
        var property = FindProperty(name, _properties, false);
        if (property != null)
        {
            property.floatValue = val;
            return true;
        }
        return false;
    }

    private void SetKeyword(string keyword, bool enabled)
    {
        if (enabled)
        {
            foreach (Material mat in _materials)
                mat.EnableKeyword(keyword);
        }
        else
        {
            foreach (Material mat in _materials)
                mat.DisableKeyword(keyword);
        }
    }
}
﻿#ifndef CUSTOM_LIGHT_INCLUDED
#define CUSTOM_LIGHT_INCLUDED

#define MAX_DIRECTIONAL_LIGHT_COUNT 4
#define MAX_OTHER_LIGHT_COUNT 16

CBUFFER_START(_CustomLight)
    int _DirectionalLightCount;
    float4 _DirectionalLightColors[MAX_DIRECTIONAL_LIGHT_COUNT];
    float4 _DirectionalLightDirections[MAX_DIRECTIONAL_LIGHT_COUNT];
    float4 _DirectionalLightShadowData[MAX_DIRECTIONAL_LIGHT_COUNT];

    int _OtherLightCount;
    float4 _OtherLightColors[MAX_OTHER_LIGHT_COUNT];
    float4 _OtherLightPositions[MAX_OTHER_LIGHT_COUNT];
CBUFFER_END

struct Light
{
    float3 direction;
    float3 color;
    float attenuation;
};

int GetDirectionalLightCount()
{
    return _DirectionalLightCount;
}

int GetOtherLightCount()
{
    return _OtherLightCount;
}

DirectionalShadowData GetDirectionalShadowData(int lightIndex, ShadowData shadowData)
{
    DirectionalShadowData data;
    data.strength = _DirectionalLightShadowData[lightIndex].x * shadowData.strength;
    data.tileIndex = _DirectionalLightShadowData[lightIndex].y + shadowData.cascadeIndex;
    data.normalBias = _DirectionalLightShadowData[lightIndex].z;
    return data;
}

Light GetDirectionLight(int idx, Surface surfaceWS, ShadowData shadowData)
{
    Light light;
    light.direction = _DirectionalLightDirections[idx].xyz;
    light.color = _DirectionalLightColors[idx].rgb;
    DirectionalShadowData dirShadowData = GetDirectionalShadowData(idx, shadowData);
    light.attenuation = GetDirectionalShadowAttenuation(dirShadowData, shadowData, surfaceWS);
    return light;
}

Light GetOtherLight(int idx, Surface surfaceWS, ShadowData shadowData)
{
    Light light;
    float3 dir = _OtherLightPositions[idx].xyz - surfaceWS.position;
    light.direction = normalize(dir);
    light.color = _OtherLightColors[idx].rgb;
    float distanceSqrt = max(dot(dir, dir), 0.001);
    float rangeAttenuation = Square(saturate(1.0 - Square(distanceSqrt * _OtherLightPositions[idx].w)));
    light.attenuation = rangeAttenuation / distanceSqrt;
    return light;
}

#endif
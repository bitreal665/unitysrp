﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class PerObjectMaterialProperties : MonoBehaviour
{
    private static readonly int BaseColorId = Shader.PropertyToID("_BaseColor");
    private static readonly int CutoffId = Shader.PropertyToID("_Cutoff");
    private static readonly int MetallicId = Shader.PropertyToID("_Metallic");
    private static readonly int SmoothnessId = Shader.PropertyToID("_Smoothness");
    private static readonly int EmissionColorId = Shader.PropertyToID("_EmissionColor");

    [SerializeField] private Color _baseColor;
    [SerializeField] private float _cutoff;
    [SerializeField] private Renderer _renderer;
    [SerializeField] [Range(0f, 1f)] private float _metallic;
    [SerializeField] [Range(0f, 1f)] private float _smoothness;
    [SerializeField] [ColorUsage(false, true)] private Color _emissionColor;

    private static MaterialPropertyBlock _materialPropertyBlock;

    private void OnValidate()
    {
        if (_renderer != null)
        {
            if (_materialPropertyBlock == null)
                _materialPropertyBlock = new MaterialPropertyBlock();
            _materialPropertyBlock.SetColor(BaseColorId, _baseColor);
            _materialPropertyBlock.SetFloat(CutoffId, _cutoff);
            _materialPropertyBlock.SetFloat(MetallicId, _metallic);
            _materialPropertyBlock.SetFloat(SmoothnessId, _smoothness);
            _materialPropertyBlock.SetColor(EmissionColorId, _emissionColor);
            _renderer.SetPropertyBlock(_materialPropertyBlock);
        }
    }
}
